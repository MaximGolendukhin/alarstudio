package com.example.alarstudios.login

import android.animation.ObjectAnimator
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.alarstudios.R
import com.example.alarstudios.databinding.FragmentLoginBinding

class LoginFragment : Fragment() {
    private val loginViewModel: LoginViewModel by lazy {
        ViewModelProvider(this).get(LoginViewModel::class.java)
    }
    private lateinit var binding: FragmentLoginBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        binding.lifecycleOwner = this

        binding.submitButton.setOnClickListener {
            loginViewModel.submit(binding.login.text.toString(), binding.password.text.toString())
        }

        handleIncorrectCredentials()
        initNavigation()

        return binding.root
    }

    private fun handleIncorrectCredentials() {
        loginViewModel.needToAnimateButton.observe(viewLifecycleOwner, {
            it?.let {
                if (it) credentialsAreIncorrectAnimation()
                else loginViewModel.needToAnimateButton.value = true
            }
        })
    }

    private fun credentialsAreIncorrectAnimation() {
        with(ObjectAnimator.ofArgb(binding.submitButton, "backgroundColor",
            requireActivity().getColor(R.color.purple_500), Color.RED)) {
            duration = 700
            repeatCount = 1
            repeatMode = ObjectAnimator.REVERSE
            start()
        }
    }

    private fun initNavigation() {
        loginViewModel.navigateToList.observe(viewLifecycleOwner, {
            it?.let {
                this.findNavController()
                    .navigate(LoginFragmentDirections.loginFragmentToListFragment(it))
                loginViewModel.displayListComplete()
            }
        })
    }
}