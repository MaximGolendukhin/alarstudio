package com.example.alarstudios.login

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.alarstudios.ResponseStatus
import com.example.alarstudios.login.network.AuthApi
import kotlinx.coroutines.runBlocking
import retrofit2.HttpException

class LoginViewModel(application: Application) : AndroidViewModel(application) {
    val needToAnimateButton = MutableLiveData<Boolean?>()
    val navigateToList = MutableLiveData<String?>()

    fun submit(username: String, password: String): ResponseStatus {
        var responseStatus = ResponseStatus.OK
        runBlocking {
            try {
                val loginResponse = AuthApi.retrofitService.auth(username, password).body()
                val code = loginResponse?.code ?: ""
                navigateToList.value = if (code == "") null else code
                needToAnimateButton.value = code == ""
            } catch (e: HttpException) {
                responseStatus =  ResponseStatus.HTTP_CONNECTION_ERROR
            }
            catch (e: Throwable) { e
                responseStatus =  ResponseStatus.ANOTHER_ERROR
            }
        }
        return responseStatus
    }

    fun displayListComplete() {
        navigateToList.value = null
    }
}