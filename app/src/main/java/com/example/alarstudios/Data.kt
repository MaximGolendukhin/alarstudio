package com.example.alarstudios

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.lang.Exception

data class CoordinatesResponse(val status: String, val page: Int, val data: List<Coordinates>)

@Parcelize
data class Coordinates(val id: String, val name: String, val country: String, val lat: String,
                       val lon: String) : Parcelable

sealed class CoordinatesResult {
    data class Success(val data: List<Coordinates>) : CoordinatesResult()
    data class Error(val error: Exception) : CoordinatesResult()
}