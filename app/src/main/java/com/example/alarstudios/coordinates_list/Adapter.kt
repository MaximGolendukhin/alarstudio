package com.example.alarstudios.coordinates_list

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.example.alarstudios.Coordinates

class Adapter(private val clickListener: ItemClickListener) :
    ListAdapter<Coordinates, CoordinatesViewHolder>(DIFF_UTIL_COMPARATOR) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CoordinatesViewHolder.from(parent)

    override fun onBindViewHolder(holder: CoordinatesViewHolder, position: Int) {
        val item = getItem(position)
        item?.let {
            holder.bind(clickListener, it)
        }
    }

    companion object {
        private val DIFF_UTIL_COMPARATOR = object : DiffUtil.ItemCallback<Coordinates>() {
            override fun areItemsTheSame(oldItem: Coordinates, newItem: Coordinates) =
                oldItem.lat == newItem.lat && oldItem.lon == newItem.lon

            override fun areContentsTheSame(oldItem: Coordinates, newItem: Coordinates) =
                oldItem == newItem
        }
    }
}

class ItemClickListener(val clickListener: (coordinates: Coordinates) -> Unit) {
    fun onClick(coordinates: Coordinates) = clickListener(coordinates)
}