package com.example.alarstudios.coordinates_list.network

import com.example.alarstudios.Coordinates
import com.example.alarstudios.CoordinatesResult
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import retrofit2.HttpException
import java.io.IOException

private const val STARTING_PAGE_INDEX = 1

class Repository(private val code: String) {
    private val inMemoryCache = mutableListOf<Coordinates>()
    private val searchResults = MutableSharedFlow<CoordinatesResult>(replay = 1)
    private var lastRequestedPage = STARTING_PAGE_INDEX
    private var isRequestInProgress = false

    suspend fun getSearchResultStream(): Flow<CoordinatesResult> {
        lastRequestedPage = 1
        inMemoryCache.clear()
        requestAndSaveData()
        return searchResults
    }

    suspend fun requestMore() {
        if (isRequestInProgress) return
        lastRequestedPage++
        requestAndSaveData()
    }

    private suspend fun requestAndSaveData(): Boolean {
        isRequestInProgress = true
        var successful = false

        try {
            val coordinates = CoordinatesApi
                .retrofitService
                .getCoordinates(code, lastRequestedPage.toString()).body()?.data ?: emptyList()
            inMemoryCache.addAll(coordinates)
            searchResults.emit(CoordinatesResult.Success(inMemoryCache))
            successful = true
        } catch (exception: IOException) {
            searchResults.emit(CoordinatesResult.Error(exception))
        } catch (exception: HttpException) {
            searchResults.emit(CoordinatesResult.Error(exception))
        }
        isRequestInProgress = false
        return successful
    }
}