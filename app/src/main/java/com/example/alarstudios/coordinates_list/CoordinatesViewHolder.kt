package com.example.alarstudios.coordinates_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.alarstudios.Coordinates
import com.example.alarstudios.databinding.ListItemBinding

class CoordinatesViewHolder private constructor(private val binding: ListItemBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(clickListener: ItemClickListener, item: Coordinates) {
        binding.coordinates = item
        binding.clickListener = clickListener
        binding.latitudeTextView.text = String.format("%.2f", item.lat.toDouble())
        binding.longitudeTextView.text = String.format("%.2f", item.lon.toDouble())
        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): CoordinatesViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ListItemBinding.inflate(layoutInflater, parent, false)
            return CoordinatesViewHolder(binding)
        }
    }
}