package com.example.alarstudios.coordinates_list

import android.app.Application
import androidx.lifecycle.*
import com.example.alarstudios.CoordinatesResult
import com.example.alarstudios.coordinates_list.network.Repository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ListViewModel(code: String, application: Application) : AndroidViewModel(application) {
    private val repository = Repository(code)

    fun getSearchResult(): LiveData<CoordinatesResult> = liveData {
            val result = repository.getSearchResultStream().asLiveData(Dispatchers.Main)
            emitSource(result)
        }

    fun listScrolled(lastVisibleItemPosition: Int, totalItemCount: Int) {
        if (lastVisibleItemPosition == totalItemCount - 1) {
            viewModelScope.launch {
                repository.requestMore()
            }
        }
    }
}