package com.example.alarstudios.coordinates_list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.alarstudios.CoordinatesResult
import com.example.alarstudios.R
import com.example.alarstudios.databinding.FragmentCoordinatesBinding

class ListFragment : Fragment() {
    private lateinit var binding: FragmentCoordinatesBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_coordinates, container, false)
        binding.lifecycleOwner = this

        val coordinatesViewModelFactory =
            CoordinatesViewModelFactory(ListFragmentArgs.fromBundle(requireArguments()).code,
                requireNotNull(activity).application
            )
        val listViewModel = ViewModelProvider(this,
            coordinatesViewModelFactory).get(ListViewModel::class.java)
        binding.listViewModel = listViewModel

        val adapter = Adapter(ItemClickListener {
            this.findNavController()
                .navigate(ListFragmentDirections.listFragmentToMapFragment(it))
        })

        listViewModel.getSearchResult().observe(viewLifecycleOwner, {
            when (it) {
                is CoordinatesResult.Success -> {
                    adapter.submitList(it.data)
                    adapter.notifyDataSetChanged()
                }
                is CoordinatesResult.Error -> {
                    Toast.makeText(requireContext(), "Patience, my friend", Toast.LENGTH_LONG).show()
                }
            }
        })
        binding.coordinatesList.adapter = adapter
        binding.coordinatesList.layoutManager = LinearLayoutManager(activity)

        setupScrollListener(listViewModel)

        return binding.root
    }

    private fun setupScrollListener(viewModel: ListViewModel) {
        val layoutManager = binding.coordinatesList.layoutManager as LinearLayoutManager
        binding.coordinatesList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val totalItemCount = layoutManager.itemCount
                val lastVisibleItem = layoutManager.findLastVisibleItemPosition()
                viewModel.listScrolled(lastVisibleItem, totalItemCount)
            }
        })
    }
}