package com.example.alarstudios.coordinates_list

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class CoordinatesViewModelFactory(private val code: String, private val application: Application) :
    ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ListViewModel::class.java)) {
            return ListViewModel(code, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}