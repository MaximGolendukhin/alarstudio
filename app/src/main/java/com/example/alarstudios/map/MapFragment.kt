package com.example.alarstudios.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.alarstudios.R
import com.example.alarstudios.databinding.FragmentMapBinding
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapFragment : Fragment(), OnMapReadyCallback {
    private lateinit var mapViewModel: MapViewModel
    private lateinit var binding: FragmentMapBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_map,
            container, false
        )
        binding.lifecycleOwner = this

        val mapViewModelFactory = MapViewModelFactory(
            MapFragmentArgs.fromBundle(requireArguments()).coordinates,
            requireNotNull(activity).application
        )

        mapViewModel = ViewModelProvider(this, mapViewModelFactory)
            .get(MapViewModel::class.java)

        val mapView = binding.map
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)

        return binding.root
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mapViewModel.coordinatesLiveData.observe(viewLifecycleOwner, {
            val mapPoint = LatLng(it.lat.toDouble(), it.lon.toDouble())
            with (googleMap) {
                mapType = GoogleMap.MAP_TYPE_NORMAL
                addMarker(MarkerOptions().position(mapPoint).title(it.name))
                moveCamera(CameraUpdateFactory.newLatLng(mapPoint))

                animateCamera(CameraUpdateFactory.newLatLngZoom(mapPoint, 14f))
            }
        })
    }
}