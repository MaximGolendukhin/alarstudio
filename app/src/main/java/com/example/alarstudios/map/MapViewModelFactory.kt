package com.example.alarstudios.map

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.alarstudios.Coordinates

class MapViewModelFactory (private val coordinates: Coordinates, private val application: Application) :
        ViewModelProvider.Factory {
        @Suppress("unchecked_cast")
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(MapViewModel::class.java)) {
                return MapViewModel(coordinates, application) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
}