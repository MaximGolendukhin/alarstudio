package com.example.alarstudios.map

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.alarstudios.Coordinates

class MapViewModel(coordinates: Coordinates, application: Application) : AndroidViewModel(application) {
    val coordinatesLiveData = MutableLiveData(coordinates)
}