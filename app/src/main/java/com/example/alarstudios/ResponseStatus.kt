package com.example.alarstudios

enum class ResponseStatus { OK, HTTP_CONNECTION_ERROR, ANOTHER_ERROR }